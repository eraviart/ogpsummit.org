server {
    listen 80;
    listen [::]:80;

    server_name www.mobile.ogpsummit.org;
    return 301 $scheme://en.2016.ogpsummit.org$request_uri;
}
