server {
    listen 80;
    listen [::]:80;

    server_name fr.ogpsummit.org;
    return 301 $scheme://fr.2016.ogpsummit.org$request_uri;
}
